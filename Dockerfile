FROM python:3.8.2

RUN apt-get update -qq && apt-get install -y -qq libxmlsec1-dev
RUN pip install clikit==0.6.2


RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
RUN ["/bin/bash", "-c", "source $HOME/.poetry/env; poetry self update 1.0.10"]
RUN ["/bin/bash", "-c", "source $HOME/.poetry/env; poetry config virtualenvs.create false"]


COPY pyproject.toml /app/
COPY poetry.lock /app/

WORKDIR /app
RUN ["/bin/bash", "-c", "source $HOME/.poetry/env; poetry install --no-dev; rm -rf `poetry config cache-dir`"]

COPY . /app
